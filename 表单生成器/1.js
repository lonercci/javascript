// ==UserScript==
// @name         大学生在线助手 - Pro
// @version      0.4
// @description  目前可以自动完成四史活动，这是包含题库的版本，需要在 telegram 加群获取 token
// @author       h
// @match        http://ssxx.univs.cn/*
// @match        https://ssxx.univs.cn/*
// @require https://cdn.jsdelivr.net/npm/ajax-hook@2.0.3/dist/ajaxhook.min.js
// @grant none
// @license        GPL3 license
// @namespace https://bukkia.pp.ua/univs/
// ==/UserScript==

const config = {
    token: '', // 在 telegram 加入 @daixuesheng 获取
    loop: false, // true 为开启循环作答，会自动开始，结束了自动点返回
    target: [4,5,6,7] // 4 英雄篇 5 复兴篇 6 创新篇 7 信念篇
}

if(config.token == ''){
    config.token = prompt('请输入token（进tg群 @daixuesheng 免费获取)')
    if(!config.token){
        alert('没有使用token，将使用固定选项回答。')
    }
}
let answer = []
let question_data = {}
ah.proxy({
    onRequest: async (config, handler) => {
        if(config.url.indexOf('/cgi-bin/race/answer/') > -1){
            let body = JSON.parse(config.body)
            if(answer.length > 0){
                body.answer = answer
                question_data.answer = answer
                config.body = JSON.stringify(body)
            }
        }
        handler.next(config)
    },
    onError: (err, handler) => {
        alert('出现错误，网路问题，可能是大学生在线网站挂了，也可能是题库挂了')
        alert(err)
        handler.next(handler)
    },
    onResponse: async (response, handler) => {
        console.log(response)
        try {
            if(response.config.url.indexOf('/cgi-bin/race/answer/') > -1){
                let body = JSON.parse(response.response)
                question_data.answer = body.data.correct_ids
                question_data.token = config.token
                if(answer.length == 0)
                    (await axios.post('https://univs.yattaze.eu.org/api/post_answer',question_data))
            }else if(response.config.url.indexOf('/cgi-bin/race/question/') > -1){
                let body = JSON.parse(response.response)
                question_data = body.data
                let cloud_answer = (await axios.post('https://univs.yattaze.eu.org/api/get_answer',{
                    id: question_data.id,
                    token: config.token
                })).data
                if(cloud_answer.ok)
                    answer = cloud_answer.data.answer
                else
                    answer = []
                if(cloud_answer.msg)
                    alert(cloud_answer.msg)
            }
        } catch (error) {
            console.error(error)
        }
        handler.next(response)
    }
})
setInterval(()=>{
    if(document.getElementsByClassName('el-checkbox__original').length > 0)
        document.getElementsByClassName('el-checkbox__original')[1].click()
    if(document.getElementsByClassName('el-radio__original').length > 0)
        document.getElementsByClassName('el-radio__original')[1].click()
    if(document.getElementsByClassName('question_btn').length > 0)
        document.getElementsByClassName('question_btn')[0].click()
    if(document.getElementsByClassName('code_span').length > 0){
        document.getElementsByClassName('el-input__inner')[0].value = document.getElementsByClassName('code_span')[0].innerText
        document.getElementsByClassName('el-input__inner')[0].dispatchEvent(new Event('input'))
        document.getElementsByClassName('common_btn2')[0].click()
    }
},2000)

if(config.loop)
    setInterval(()=>{
        if(document.getElementsByTagName('img').length > 6)
            document.getElementsByTagName('img')[config.target[Math.floor(Math.random() * config.target.length)]].click()
        if(document.getElementsByClassName('result_back_btn').length > 0)
            document.getElementsByClassName('result_back_btn')[0].click()
    },2000)