function createCalendar(year) {
	var htmlStr = "";
	var weekday = new Date(year, 0, 1).getDay();//这一年的元旦星期几？
	for(let month = 1; month<=12; month++) {
		htmlStr += "<table>";
		htmlStr += "<tr><th colspan='7'>"+year+"年"+month+"月</th></tr>";
		htmlStr += "<td>日</td><td>一</td><td>二</td><td>三</td><td>四</td><td>五</td><td>六</td>"
		var max = new Date(year, month, 0).getDate();//每个月的天数，0表示上月最后一天
		htmlStr += "<tr>";
		for(let day=1; day<=max; day++) {
			if(weekday!=0 && day==1) {//1号不是星期天
				htmlStr += "<td colspan='"+weekday+"'></td>";
			}
			htmlStr += "<td>"+day+"</td>";
			if(weekday==6 && day!=max) {//星期六但不是该月最后一天
				htmlStr += "</tr><tr>";
			} else if(day == max) {
				htmlStr += "</tr>";
			}
			weekday = ((++weekday>6)? 0: weekday);
		}
		htmlStr += "</table>";
	}
	return htmlStr;
}